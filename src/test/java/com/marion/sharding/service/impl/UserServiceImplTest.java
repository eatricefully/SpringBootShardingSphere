package com.marion.sharding.service.impl;

import com.marion.sharding.entity.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceImplTest {

    @Autowired
    private UserServiceImpl userService;

    @Test
    void save() {
        for (int i = 0; i < 100; i++) {
            userService.save(i + 1, "name" + i, (int) (Math.random() * 100));
        }
    }

    @Test
    void findAll() {
        List<UserEntity> list = userService.findAll();
        System.out.println(list);
    }
}