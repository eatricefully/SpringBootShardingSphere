package com.marion.sharding.service.impl;

import com.marion.sharding.entity.UserEntity;
import com.marion.sharding.repo.UserRepo;
import com.marion.sharding.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public void save(int id, String name, int age) {
        UserEntity entity = UserEntity.builder()
                .id(id)
                .name(name)
                .age(age)
                .build();
        userRepo.save(entity);
    }

    @Override
    public List<UserEntity> findAll() {
        return userRepo.findAll();
    }
}
