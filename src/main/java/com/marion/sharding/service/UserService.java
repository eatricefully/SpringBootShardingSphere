package com.marion.sharding.service;

import com.marion.sharding.entity.UserEntity;

import java.util.List;

public interface UserService {

    void save(int id, String name, int age);

    List<UserEntity> findAll();
}
