package com.marion.sharding.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    /**
     * 1. 写入user信息
     */
    @GetMapping("/save")
    public Object saveUser() {
        return null;
    }

    /**
     * 2. 查询用户列表
     */
    @GetMapping("/list")
    public List<Object> userList() {
        return new ArrayList<>();
    }

}
